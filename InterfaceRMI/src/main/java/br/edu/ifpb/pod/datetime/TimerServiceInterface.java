package br.edu.ifpb.pod.datetime;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;

/**
 *
 * @author vagner
 * https://bitbucket.org/vagnerbarbosa/rmi
 */
public interface TimerServiceInterface extends Remote {
    
    public Date getDateTime() throws RemoteException;
    public void upDateDateTime(Date d) throws RemoteException;
    
}
