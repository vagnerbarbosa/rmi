package br.edu.ifpb.pod.somador;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author vagner
 * https://bitbucket.org/vagnerbarbosa/rmi
 */
public interface SomadorInterface extends Remote {
    
    public Integer soma(String valor) throws RemoteException;
    
}
