package br.edu.ifpb.pod.somador;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author vagner
 * https://bitbucket.org/vagnerbarbosa/rmi
 */
public class Somador extends UnicastRemoteObject implements SomadorInterface {

    public Somador() throws RemoteException {
        super();
    }

    @Override
    public Integer soma(String valor) throws RemoteException {
        int retorno = 0;
        if (valor.contains(":")) {
            String st = valor.replace("[", "").replace("]", "");
            String[] valores = st.split(":");
            Integer referencia = Integer.valueOf(valores[0].trim());
            Integer primeiro = Integer.valueOf(valores[1].trim());
            Integer segundo = Integer.valueOf(valores[2].trim());
            retorno = primeiro + segundo;
        }
        return retorno;
    }
}
