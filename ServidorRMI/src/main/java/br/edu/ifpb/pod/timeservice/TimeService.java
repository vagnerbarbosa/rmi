package br.edu.ifpb.pod.timeservice;

import br.edu.ifpb.pod.datetime.TimerServiceInterface;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;

/**
 *
 * @author vagner
 * https://bitbucket.org/vagnerbarbosa/rmi
 */
public class TimeService extends UnicastRemoteObject implements TimerServiceInterface {
    
    private Date dateTime;
    private Date diferencaData;

    public TimeService() throws RemoteException {
        super();
    }

    @Override
    public Date getDateTime() throws RemoteException {        
        return this.dateTime = new Date();
    }
    
    @Override
    public void upDateDateTime(Date d) throws RemoteException {
        diferencaData.compareTo(d);
    }




    
}
