package br.edu.ifpb.pod.somador;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;

/**
 *
 * @author vagner
 * https://bitbucket.org/vagnerbarbosa/rmi
 */
public class ServidorSomador {
    
    public static void main(String[] args) throws RemoteException, AlreadyBoundException {
        Registry registry = LocateRegistry.createRegistry(10999);
        
        Somador service = new Somador();
        
        registry.bind("Somador", service);
        
        System.out.println(Arrays.toString(registry.list()));
        

                
                
        
        
        
    }
    
}
