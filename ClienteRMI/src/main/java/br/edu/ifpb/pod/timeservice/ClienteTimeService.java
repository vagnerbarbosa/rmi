package br.edu.ifpb.pod.timeservice;

import br.edu.ifpb.pod.datetime.TimerServiceInterface;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author vagner
 * https://bitbucket.org/vagnerbarbosa/rmi
 */
public class ClienteTimeService {
    
    public static void main(String[] args) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry("localhost", 10999);
        TimerServiceInterface service = (TimerServiceInterface) registry.lookup("TimeService");
        
        System.out.println(service.getDateTime().toString());
    }
    
}
