package br.edu.ifpb.pod.somador;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author vagner
 * https://bitbucket.org/vagnerbarbosa/rmi
 */
public class ClienteSomador {
    
    public static void main(String[] args) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry("localhost", 10999);
        SomadorInterface resultado =  (SomadorInterface) registry.lookup("Somador");
        
        System.out.println(resultado.soma("[1:2:3]"));
        
        
        
        
    }
    
}
